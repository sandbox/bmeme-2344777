<?php

/**
 * @file
 * Dynamic Select pages.
 */

/**
 * Menu callback; Ajax callback for dynamic_select deletions.
 *
 * This rebuilds the form element for a particular field item. As long as the
 * form processing is properly encapsulated in the widget element the form
 * should rebuild correctly using FAPI without the need for additional callbacks
 * or processing.
 */
function dynamic_select_ajax_delete() {
  $form_parents = func_get_args();
  $form_build_id = (string) array_pop($form_parents);
  $delta = array_pop($form_parents);

  if (empty($_POST['form_build_id']) || $form_build_id != $_POST['form_build_id']) {
    // Invalid request.
    drupal_set_message(t('An unrecoverable error occurred during the deletion of the dynamic_select field.'), 'error');
    $commands = array();
    $commands[] = ajax_command_replace(NULL, theme('status_messages'));
    return array('#type' => 'ajax', '#commands' => $commands);
  }

  list($form, $form_state) = ajax_get_form();

  if (!$form) {
    // Invalid form_build_id.
    drupal_set_message(t('An unrecoverable error occurred. Use of this form has expired. Try reloading the page and submitting again.'), 'error');
    $commands = array();
    $commands[] = ajax_command_replace(NULL, theme('status_messages'));
    return array('#type' => 'ajax', '#commands' => $commands);
  }

  $field = drupal_array_get_nested_value($form, $form_parents);
  $items_count = 0;

  foreach ($field as $key => $value) {
    if (is_numeric($key)) {
      $items_count++;
    }
  }

  if ($items_count > 0) {
    // Process user input. $form and $form_state are modified in the process.
    drupal_process_form($form['#form_id'], $form, $form_state);

    $output = theme('status_messages') . drupal_render($field[$delta]);

    $js = drupal_add_js();
    $settings = call_user_func_array('array_merge_recursive', $js['settings']['data']);

    $commands = array();
    $commands[] = ajax_command_replace(NULL, $output, $settings);
    $commands[] = ajax_command_restripe('table[id^="' . $field['#field_name'] . '-values"]');

    return array('#type' => 'ajax', '#commands' => $commands);
  }
}