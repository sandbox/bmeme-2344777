<?php

/**
 * @file Webform Component information for a dynamic_select field type 
 */

/**
 * Specify the default properties of a component.
 * @return
 *   An array defining the default structure of a component.
 */
function _webform_defaults_dynamic_select() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'required' => 0,
    'mandatory' => 0,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'extra' => array(
      'title_display' => 0,
      'multiple' => FALSE,
      'width' => '',
      'disabled' => FALSE,
      'private' => FALSE,
      'attributes' => array(),
      'description' => '',
      'vocabulary' => NULL,
      'starting_level' => 0,
      'nesting_levels' => 0,
      'free_save' => FALSE,
      'order_by' => DYNAMIC_SELECT_ORDER_BY_WEIGHT
    ),
  );
}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_dynamic_select() {
  return array(
    'webform_dynamic_select' => array(
      'render element' => 'element',
      'file' => 'webform_dynamic_select.components.inc',
      'path' => drupal_get_path('module', 'webform_dynamic_select') . '/includes',
    ),
  );
}

/**
 * Generate the form for editing a component.
 * Create a set of form elements to be displayed on the form for editing this
 * component. Use care naming the form items, as this correlates directly to the
 * database schema. The component "Name" and "Description" fields are added to
 * every component type and are not necessary to specify here (although they
 * may be overridden if desired).
 *
 * @param $component
 *   A Webform component array.
 *
 * @return
 *   An array of form items to be displayed on the edit component page
 */
function _webform_edit_dynamic_select($component) {
  $form = array();

  $form['extra']['vocabulary'] = array(
    '#type' => 'select',
    '#options' => range(1, 2),
    '#title' => t('Vocabulary'),
    '#default_value' => $component['extra']['vocabulary'],
  );

  $form['extra']['starting_level'] = array(
    '#type' => 'select',
    '#options' => range(1, 2),
    '#title' => t('Starting level'),
    '#default_value' => $component['extra']['starting_level'],
  );

  $form['extra']['nesting_levels'] = array(
    '#type' => 'select',
    '#options' => range(0, 10),
    '#title' => t('Dial Code/Prefix separator'),
    '#default_value' => $component['extra']['nesting_levels'],
  );

  $form['extra']['free_save'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow users to save terms from any level'),
    '#default_value' => $component['extra']['free_save'],
  );

  $form['extra']['order_by'] = array(
    '#type' => 'select',
    '#options' => dynamic_select_get_order_options(),
    '#title' => t('Order terms by:'),
    '#default_value' => $component['extra']['order_by'],
  );

  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Default value'),
    '#default_value' => $component['value'],
    '#description' => t('The default value of the field.') . theme('webform_token_help'),
    '#size' => 60,
    '#maxlength' => 1024,
    '#weight' => 0,
  );

  $form['display']['disabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disabled'),
    '#return_value' => 1,
    '#description' => t('Make this field non-editable. Useful for setting an unchangeable default value.'),
    '#weight' => 11,
    '#default_value' => $component['extra']['disabled'],
    '#parents' => array('extra', 'disabled'),
  );

  return $form;
}

/**
 * Render a Webform component to be part of a form.
 *
 * @param $component
 *   A Webform component array.
 * @param $value
 *   If editing an existing submission or resuming a draft, this will contain
 *   an array of values to be shown instead of the default in the component
 *   configuration. This value will always be an array, keyed numerically for
 *   each value saved in this field.
 * @param $filter
 *   Whether or not to filter the contents of descriptions and values when
 *   rendering the component. Values need to be unfiltered to be editable by
 *   Form Builder.
 *
 * @see _webform_client_form_add_component()
 */
function _webform_render_dynamic_select($component, $value = NULL, $filter = TRUE) {

  $element = array(
    '#type' => 'dynamic_select',
    '#title' => $filter ? _webform_filter_xss($component['name']) : $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#required' => $component['mandatory'],
    '#default_value' => $value,
    '#theme_wrappers' => array('webform_element'),
    '#process' => array('webform_process_dynamic_select'),
    '#weight' => $component['weight'],
    '#translatable' => array('title'),
  );

  if ($component['extra']['disabled']) {
    if ($filter) {
      $element['#attributes']['readonly'] = 'readonly';
    }
    else {
      $element['#disabled'] = TRUE;
    }
  }

  return $element;
}

/**
 * Process callback for webform dynamic_select field.
 */
function webform_process_dynamic_select($element, $form_state, $form) {
  $element['#attached']['css'] = array(drupal_get_path('module', 'dynamic_select') . '/css/dynamic_select.css');

  $display_settings = $element['#webform_component']['extra'];
  
  $settings = array(
    'title' => '',
    'title_display' => 'invisible',
    'parents' => $element['#parents'],
    'required' => isset($element['#required']) ? $element['#required'] : $element['#webform_component']['mandatory'],
    'levels' => isset($element['#nesting_levels']) ? $element['#nesting_levels'] : $display_settings['nesting_levels'],
    'free_save' => isset($element['#free_save']) ? $element['#free_save'] : $display_settings['free_save'],
    'starting_level' => isset($element['#starting_level']) ? $element['#starting_level'] : $display_settings['starting_level'],
    'order_by' => isset($element['#order_by']) ? $element['#order_by'] : $display_settings['order_by'],
    'vocabulary_name' => isset($element['#vocabulary']) ? $element['#vocabulary'] : $display_settings['vocabulary'],
    'wrapper' => $element['#id'],
    'rebuildable' => FALSE
  );

  if (empty($settings['vocabulary_name'])) {
    $element['#field_prefix'] = t('Please configure this field');
    return $element;
  }

  dynamic_select_terms($settings, $element, $form_state);

  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_dynamic_select($component, $value, $format = 'html') {
  return array(
    '#title' => $component['name'],
    '#weight' => $component['weight'],
    '#theme' => 'webform_dynamic_select',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#post_render' => array('webform_element_wrapper'),
    '#component' => $component,
    '#format' => $format,
    '#value' => $value,
  );
}

/**
 * Formats a dynamic_select field widget.
 */
function theme_webform_dynamic_select($variables) {
  $markup = array();
  $element = $variables['element'];

  $term = taxonomy_term_load($element['#value']['tid']);
  $markup['#markup'] = $term->name;

  return drupal_render($markup);
}

/**
 * Implements _webform_analysis_component().
 */
function _webform_analysis_dynamic_select($component, $sids = array()) {
  $query = db_select('webform_submitted_data', 'wsd', array('fetch' => PDO::FETCH_ASSOC))
      ->fields('wsd')
      ->condition('nid', $component['nid'])
      ->condition('cid', $component['cid']);

  if (count($sids)) {
    $query->condition('sid', $sids, 'IN');
  }

  $nonblanks = 0;
  $submissions = 0;

  $result = $query->execute();

  foreach ($result as $data) {
    $filled = drupal_strlen(trim($data['data'])) > 0;
    switch ($data['no']) {
      case 'tid':
        if ($filled) {
          $nonblanks++;
        }

        $submissions++;
        break;
    }
  }

  $rows[0] = array(t('Left Blank'), ($submissions - $nonblanks));
  $rows[1] = array(t('User entered value'), $nonblanks);

  return $rows;
}

function webform_dynamic_select_render_value($component, $value) {
  $markup = array();
  $output = '';

  if (!empty($value['tid'])) {
    $term = taxonomy_term_load($value['tid']);
    $output = $term->name;
  }

  $markup['#markup'] = $output;

  return drupal_render($markup);
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_dynamic_select($component, $value) {
  return webform_dynamic_select_render_value($component, $value);
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_dynamic_select($component, $export_options) {
  $header = array();
  $header[0] = '';
  $header[1] = '';
  $header[2] = $component['name'];
  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_dynamic_select($component, $export_options, $value) {
  return webform_dynamic_select_render_value($component, $value);
}

/**
 * Implements _form_builder_webform_form_builder_map_component().
 */
function _form_builder_webform_form_builder_map_dynamic_select() {
  return array(
    'form_builder_type' => 'dynamic_select',
    'properties' => array(
      'vocabulary' => array(
        'form_parents' => array('display', 'vocabulary'),
        'storage_parents' => array('extra', 'vocabulary')
      ),
      'starting_level' => array(
        'form_parents' => array('display', 'starting_level'),
        'storage_parents' => array('extra', 'starting_level')
      ),
      'nesting_levels' => array(
        'form_parents' => array('display', 'nesting_levels'),
        'storage_parents' => array('extra', 'nesting_levels')
      ),
      'free_save' => array(
        'form_parents' => array('display', 'free_save'),
        'storage_parents' => array('extra', 'free_save')
      ),
      'order_by' => array(
        'form_parents' => array('display', 'order_by'),
        'storage_parents' => array('extra', 'order_by')
      ),
    ),
  );
}