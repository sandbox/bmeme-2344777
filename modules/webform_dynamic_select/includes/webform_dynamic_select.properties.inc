<?php

/**
 * @file webform_dynamic_select.properties.inc
 * Implementations of hook_form_builder_properties in separate functions.
 */

/**
 * Configuration form for the "vocabulary" property.
 */
function form_builder_property_vocabulary_form(&$form_state, $form_type, $element, $property) {
  $form = array();

  foreach (taxonomy_vocabulary_get_names() as $vocabulary) {
    $vocabularies[$vocabulary->machine_name] = $vocabulary->name;
  }

  $form['vocabulary'] = array(
    '#form_builder' => array('property_group' => 'options'),
    '#title' => t('Vocabulary'),
    '#type' => 'select',
    '#options' => $vocabularies,
    '#default_value' => $element['#webform_component']['extra'][$property],
    '#weight' => -10,
    '#required' => TRUE
  );

  return $form;
}

/**
 * Configuration form for the "starting_level" property.
 */
function form_builder_property_starting_level_form(&$form_state, $form_type, $element, $property) {
  $form = array();

  $levels = array(0 => t('<root>'));

  if (!empty($element['#webform_component']['extra']['vocabulary'])) {
    $field = array(
      'settings' => array(
        'allowed_values' => array(
          array(
            'vocabulary' => $element['#webform_component']['extra']['vocabulary'],
            'parent' => 0
          )
        )
      )
    );

    $levels = $levels + taxonomy_allowed_values($field);
  }

  $form['starting_level'] = array(
    '#form_builder' => array('property_group' => 'options'),
    '#title' => t('Set the starting level'),
    '#type' => 'select',
    '#options' => $levels,
    '#default_value' => $element['#webform_component']['extra'][$property],
    '#weight' => -9,
  );

  return $form;
}

/**
 * Configuration form for the "nesting_levels" property.
 */
function form_builder_property_nesting_levels_form(&$form_state, $form_type, $element, $property) {
  $form = array();

  $form['nesting_levels'] = array(
    '#form_builder' => array('property_group' => 'options'),
    '#title' => t('Levels limit'),
    '#type' => 'select',
    '#options' => range(0, 10),
    '#default_value' => $element['#webform_component']['extra'][$property],
    '#weight' => -8,
    '#description' => t('1 level = 1 select. Leave 0 for no nesting levels control.')
  );

  return $form;
}

/**
 * Configuration form for the "free_save" property.
 */
function form_builder_property_free_save_form(&$form_state, $form_type, $element, $property) {
  $form = array();

  $form['free_save'] = array(
    '#form_builder' => array('property_group' => 'options'),
    '#title' => t('Allow users to save terms from any level'),
    '#type' => 'checkbox',
    '#default_value' => $element['#webform_component']['extra'][$property],
    '#weight' => -7,
  );

  return $form;
}

/**
 * Configuration form for the "order_by" property.
 */
function form_builder_property_order_by_form(&$form_state, $form_type, $element, $property) {
  $form = array();

  $form['order_by'] = array(
    '#form_builder' => array('property_group' => 'options'),
    '#title' => t('Order terms by:'),
    '#type' => 'select',
    '#options' => dynamic_select_get_order_options(),
    '#default_value' => $element['#webform_component']['extra'][$property],
    '#weight' => -6,
  );

  return $form;
}