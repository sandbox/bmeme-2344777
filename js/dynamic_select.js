(function($) {
    /**
     * Remove duplicated weight field appeared during ajax reload.
     */
    Drupal.behaviors.dynamicSelect = {
	attach: function(context, settings) {
	    // Remove duplicated weight field.
	    $('div.form-type-dynamic-select').find('div[class$="--weight"]').remove();

	    // Add class to each row for future field deletion.
	    $(".dynamic-select-wrapper.tid").each(function() {
		$(this).parents('tr.draggable').addClass('dynamic-select-multiple-values');
	    });

	    // Hide removed fields.
	    $('.form-item.form-type-dynamic-select').each(function() {
		if (!$(this).find('.dynamic-select-wrapper.tid').length) {
		    $(this).parents('tr.dynamic-select-multiple-values').hide();
		}
	    });

	    // restripe tables.
	    $('> tbody > tr:visible, > tr:visible', $('table.field-multiple-table'))
		.removeClass('odd even')
		.filter(':even').addClass('odd').end()
		.filter(':odd').addClass('even');
	}
    };
})(jQuery);
